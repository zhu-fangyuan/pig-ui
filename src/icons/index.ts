import SvgIcon from './index.vue'

function requireAll(requireContext) {
  Object.keys(requireContext).map((item) => {
    return item;
  });
}

const req = import.meta.globEager("./svg/*.svg");

function setSvgIcon(app){
  app.component('svg-icon',SvgIcon)
  requireAll(req);
}

export default setSvgIcon
