function modules(app) {
  const lib = import.meta.glob("./lib/*.vue");
  for (const path in lib) {
    lib[path]().then((mod) => {
      const _component = mod.default;
      const name = mod.default.name;
      app.component(name, _component);
    });
  }


  //第二种写法

  // Object.keys(lib).forEach((path)=>{
  //   lib[path]().then((mod)=>{
  //     const _component = mod.default;
  //     const name = mod.default.name;
  //     app.component(name, _component);
  //   })
  // })
}
export default modules;
