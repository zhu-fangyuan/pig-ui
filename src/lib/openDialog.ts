import Dialog from "./Dialog.vue";
import { createApp, h } from "vue";
export const openDialog = (options) => {
  const { visible, title, content, ok, cancel } = options;
  const div = document.createElement("div");
  document.body.appendChild(div);
  const close = () => {
    dialog.unmount();
    div.remove();
  };
  const dialog = createApp({
    render() {
      return h(
        Dialog,
        {
          visible: visible,
          "onUpdate:visible": (newValue) => {
            if (newValue === false) {
              close();
            }
          },
          ok,
          cancel,
        },
        {
          title,
          content,
        }
      );
    },
  });
  dialog.mount(div);
};
