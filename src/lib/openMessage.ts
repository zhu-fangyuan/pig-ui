import Message from "./Message.vue";
import { createApp, h } from "vue";
export const openMessage = (options) => {
  const { content, type, showClose } = options;
  const div = document.createElement("div");
  document.body.appendChild(div);
  const action = () => {
    message.unmount();
    div.remove();
  };
  const message = createApp({
    render() {
      return h(
        Message,
        {
          type: type,
          showClose: showClose,
          action,
        },
        {
          content: content,
        }
      );
    },
  });
  message.mount(div);
};
