import Notification from "./Notification.vue";
import { createApp, h } from "vue";
export const openNotification = (options) => {
  const { title, content, type, showClose, onClose } = options;
  const div = document.createElement("div");
  const $app = document.querySelector("#app");
  $app.appendChild(div);
  const action = () => {
    notification.unmount();
    div.remove();
  };
  const notification = createApp({
    render() {
      return h(
        Notification,
        {
          type: type,
          showClose: showClose,
          onClose,
          action,
        },
        {
          content: content,
          title: title,
        }
      );
    },
  });
  notification.mount(div);
};
