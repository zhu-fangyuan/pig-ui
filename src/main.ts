import "./lib/style/pig.scss";
import "./index.scss";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index";
import modules from "./lib";
import store from "./store/index";
import setSvgIcon from './icons/index'
import './icons/index';
import 'github-markdown-css'

const app = createApp(App);
setSvgIcon(app)
modules(app);
app.use(router);
app.use(store);
app.mount("#app");
