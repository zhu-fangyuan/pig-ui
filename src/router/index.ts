import { createWebHashHistory, createRouter } from "vue-router";

const history = createWebHashHistory();
export const asyncRoutes = [
  {
    path: "/",
    name: "Home",
    meta: { title: "首页" },
    component: () => import("../view/Home.vue"),
  },
  {
    path: "/doc",
    name: "Doc",
    redirect: { name: "Switch", path: "switch" },
    component: () => import("../view/Doc.vue"),
    children: [
      {
        path:'install',
        name:'Install',
        component:()=>import('../view/Install.vue')
      },
      {
        path: "switch",
        name: "Switch",
        component: () => import("../components/SwitchDemo.vue"),
      },
      {
        path: "button",
        name: "Button",
        component: () => import("../components/ButtonDemo.vue"),
      },
      {
        path: "dialog",
        name: "Dialog",
        component: () => import("../components/DialogDemo.vue"),
      },
      {
        path: "tabs",
        name: "Tabs",
        component: () => import("../components/TabsDemo.vue"),
      },
      {
        path: "message",
        name: "Message",
        component: () => import("../components/MessageDemo.vue"),
      },
      {
        path: "notification",
        name: "Notification",
        component: () => import("../components/NotificationDemo.vue"),
      },
      {
        path: "input",
        name: "Input",
        component: () => import("../lib/Input.vue"),
      },
    ],
  },
];
const router = createRouter({
  history: history,
  routes: asyncRoutes,
});

/**
 * 修改页面标题
 * @param {string} title page title
 */
function modifyTitle(title) {
  if (!title) return (document.title = "Pig-Ui");
  document.title = title;
}
router.afterEach((to, from) => {
  modifyTitle(to.meta.title);
});

export default router;
