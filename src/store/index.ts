import { createStore } from "vuex";
import routers from "./modules/routers";

const store = createStore({
  modules: {
    routers,
  },
});

export default store;
