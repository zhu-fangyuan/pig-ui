import { asyncRoutes } from "../../router/index";
const user = {
  state: () => ({
    routers: asyncRoutes,
  }),
  getters: {},
  mutations: {},
  actions: {},
};

export default user;
