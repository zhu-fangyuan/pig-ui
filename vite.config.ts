import { defineConfig } from "vite";
import { svgBuilder } from './public/svgBuilder';
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    svgBuilder('./src/icons/svg/')
  ],
});
